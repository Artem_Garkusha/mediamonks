#Android Assignment


## Why?

We are interested in your skills as a developer. As part of our assessment, we want to see what you can do.  The assignment consists of two parts - implementation and presentation of your solution and rationale to your future colleagues. There are no trick requirements here.

## Instructions

Build a stand alone native app that tells the user the weather conditions where they currently are.
The user can additionally enter a city name and also see the weather for that location too.

## The flow

App opens with (screen 1) showing the weather in the current location. If you tap on the input field, a city name can be entered and then both results are shown (screen 2).

The weather information comes from this api

http://openweathermap.org/current#list

An example call using the api

http://api.openweathermap.org/data/2.5/weather?q=zoetermeer

The weather icons can be taken from

 http://openweathermap.org/weather-conditions 

but the icon should be fetched at runtime.


## Screens

Very rough wireframes of Screen 1 and Screen 2 can be found in this project


## The Constraints

Use proper object-orientated design

Include unit testing

Support API 14 and above