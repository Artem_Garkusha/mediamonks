package gark.ebaytask.receivers;

import android.os.Bundle;
import android.os.Handler;

import gark.ebaytask.model.WeatherResponse;
import gark.ebaytask.network.requesthelper.RequestHelper;
import gark.ebaytask.ui.ItemAdapter;

/**
 * Receiver passes data result about city weather.
 */
public class CityNameResultReceiver extends BaseResultReceiver {

    public CityNameResultReceiver(ItemAdapter itemsAdapter, Handler handler) {
        super(itemsAdapter, handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        if (resultCode == RequestHelper.SUCCESS_RESULT) {
            final WeatherResponse weatherResponse = resultData.getParcelable(RequestHelper.RESULT_DATA_KEY);
            mItemsAdapter.addCity(weatherResponse);
        }
    }
}
