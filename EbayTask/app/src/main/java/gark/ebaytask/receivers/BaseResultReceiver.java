package gark.ebaytask.receivers;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import gark.ebaytask.ui.ItemAdapter;


/**
 * Generic interface for receiving a callback result from {@link gark.ebaytask.network.NetworkIntentService}
 * to main UI {@link gark.ebaytask.ui.MainActivity}.
 */
public class BaseResultReceiver extends ResultReceiver {

    protected ItemAdapter mItemsAdapter;

    public BaseResultReceiver(final ItemAdapter itemsAdapter, final Handler handler) {
        super(handler);
        mItemsAdapter = itemsAdapter;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
    }
}
