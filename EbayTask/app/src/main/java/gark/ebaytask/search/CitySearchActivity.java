package gark.ebaytask.search;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import gark.ebaytask.R;

/**
 * Activity work with Google Api to retrieve predictions of city names by some filter.
 */
public class CitySearchActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, AdapterView.OnItemClickListener {


    public static final int SEARCH_REQUEST_CODE = 27;
    public static final String CITY_KEY = "city_key";

    private static final LatLngBounds BOUNDS = new LatLngBounds(new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));
    private PlaceAutocompleteAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        setContentView(R.layout.city_search);

        final AutoCompleteTextView autocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        mAdapter = new PlaceAutocompleteAdapter(this, googleApiClient, BOUNDS, null);
        autocompleteView.setOnItemClickListener(this);
        autocompleteView.setAdapter(mAdapter);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final AutocompletePrediction item = mAdapter.getItem(position);
        final CharSequence primaryText = item.getPrimaryText(null);

        final Bundle bundle = new Bundle();
        bundle.putString(CitySearchActivity.CITY_KEY, primaryText.toString());

        final Intent intent = new Intent();
        intent.putExtras(bundle);

        setResult(RESULT_OK, intent);

        finish();
    }
}
