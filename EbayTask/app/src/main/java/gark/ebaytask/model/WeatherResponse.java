package gark.ebaytask.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * WeatherResponse class represents weather response data.
 */
public class WeatherResponse implements Parcelable {

    public String id;
    public String name;
    public String cod;
    public WeatherItem[] weather;

    public WeatherResponse() {
    }

    protected WeatherResponse(Parcel in) {
        id = in.readString();
        name = in.readString();
        cod = in.readString();
        weather = (WeatherItem[]) in.readParcelableArray(getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(cod);
        dest.writeParcelableArray(weather, flags);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<WeatherResponse> CREATOR = new Parcelable.Creator<WeatherResponse>() {
        @Override
        public WeatherResponse createFromParcel(Parcel in) {
            return new WeatherResponse(in);
        }

        @Override
        public WeatherResponse[] newArray(int size) {
            return new WeatherResponse[size];
        }
    };
}
