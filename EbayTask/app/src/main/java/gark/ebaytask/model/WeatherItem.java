package gark.ebaytask.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * WeatherItem class represents main data of weather of location.
 */
public class WeatherItem implements Parcelable {
    public String id;
    public String main;
    public String description;
    public String icon;

    protected WeatherItem(Parcel in) {
        id = in.readString();
        main = in.readString();
        description = in.readString();
        icon = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(main);
        dest.writeString(description);
        dest.writeString(icon);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<WeatherItem> CREATOR = new Parcelable.Creator<WeatherItem>() {
        @Override
        public WeatherItem createFromParcel(Parcel in) {
            return new WeatherItem(in);
        }

        @Override
        public WeatherItem[] newArray(int size) {
            return new WeatherItem[size];
        }
    };
}
