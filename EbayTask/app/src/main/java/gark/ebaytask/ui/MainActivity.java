package gark.ebaytask.ui;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import gark.ebaytask.R;
import gark.ebaytask.model.WeatherResponse;
import gark.ebaytask.network.NetworkIntentService;
import gark.ebaytask.receivers.CityNameResultReceiver;
import gark.ebaytask.receivers.CurrentLocationResultReceiver;
import gark.ebaytask.search.CitySearchActivity;

/**
 * Main application activity contains current location weather item and list of selected cities.
 */
public class MainActivity extends BaseLocationActivity implements ItemAdapter.ActionsCallback {

    private static final String ITEM_STATE = "ITEM_STATE";

    private final ArrayList<WeatherResponse> mItemsMap = new ArrayList<>();

    private ResultReceiver mCurrentLocationResultReceiver;
    private ResultReceiver mCityNameResultReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.items_list);
        recyclerView.setHasFixedSize(true);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        restoreFromInstanceState(savedInstanceState);

        final ItemAdapter itemsAdapter = new ItemAdapter(this, mItemsMap);
        recyclerView.setAdapter(itemsAdapter);

        mCurrentLocationResultReceiver = new CurrentLocationResultReceiver(itemsAdapter, new Handler());
        mCityNameResultReceiver = new CityNameResultReceiver(itemsAdapter, new Handler());
    }

    private void restoreFromInstanceState(final Bundle bundle) {
        if (bundle == null) {
            mItemsMap.add(0, null);
        } else {
            extractListFromBundle(bundle);
        }
    }

    private void extractListFromBundle(Bundle bundle) {
        final ArrayList<WeatherResponse> list = bundle.getParcelableArrayList(ITEM_STATE);
        if (list != null) {
            mItemsMap.addAll(list);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelableArrayList(ITEM_STATE, mItemsMap);
    }

    @Override
    protected void onCurrentLocationReady(final Location location) {
        // don`t request current weather if data already exist
        if (mItemsMap.get(0) == null) {
            NetworkIntentService.requestWeatherCurrentLocation(location, this, mCurrentLocationResultReceiver);
        }
    }

    private void requestWeatherByName(final String name) {
        NetworkIntentService.requestWeatherByCityName(name, this, mCityNameResultReceiver);
    }

    @Override
    public void addCitySelected(final View view) {
        final Intent intent = new Intent(this, CitySearchActivity.class);
        final String transitionKey = getString(R.string.search_transition);
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, transitionKey);
        ActivityCompat.startActivityForResult(this, intent, CitySearchActivity.SEARCH_REQUEST_CODE, options.toBundle());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CitySearchActivity.SEARCH_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    final String cityName = data.getStringExtra(CitySearchActivity.CITY_KEY);
                    requestWeatherByName(cityName);
                }
                break;
        }
    }
}
