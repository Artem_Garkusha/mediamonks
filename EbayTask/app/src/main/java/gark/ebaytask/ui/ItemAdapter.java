package gark.ebaytask.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import gark.ebaytask.R;
import gark.ebaytask.model.WeatherItem;
import gark.ebaytask.model.WeatherResponse;
import gark.ebaytask.network.RetrofitClient;

/**
 * Weather data adapter, ties data with view.
 */
public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    public static final int TYPE_DEFAULT_WEATHER_ITEM = 0;
    public static final int TYPE_BOTTOM_ITEM = 1;
    private static final int EXTRA_ITEM_COUNT = 1;

    private final ArrayList<WeatherResponse> mItemsList;
    private final ActionsCallback mActionsCallback;

    /**
     * Adapters provide a binding from an weather data set to views that are displayed
     *
     * @param actionsCallback callback from adapter to activity
     * @param itemsList       data set
     */
    public ItemAdapter(final ActionsCallback actionsCallback, final ArrayList<WeatherResponse> itemsList) {
        mActionsCallback = actionsCallback;
        mItemsList = itemsList;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mItemsList.size()) ? TYPE_BOTTOM_ITEM : TYPE_DEFAULT_WEATHER_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_BOTTOM_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_item_wether, parent, false);
                view.setOnClickListener(this);
                return new AddNewItemViewHolder(view);
            default:
            case TYPE_DEFAULT_WEATHER_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wether, parent, false);
                return new WeatherItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_BOTTOM_ITEM:
                break;
            case TYPE_DEFAULT_WEATHER_ITEM:
                final WeatherItemViewHolder item = (WeatherItemViewHolder) holder;
                fillWeatherCellItemWithData(item, position);
                break;
        }
    }

    private void fillWeatherCellItemWithData(final WeatherItemViewHolder holder, final int position) {
        final WeatherResponse weatherResponse = mItemsList.get(position);
        if (weatherResponse != null && weatherResponse.weather != null) {
            fillItemWithValidData(holder, weatherResponse);
        } else {
            fillItemWithInvalidData(holder);
        }
    }

    private void fillItemWithInvalidData(WeatherItemViewHolder holder) {
        holder.description.setText("");
        holder.text.setText(R.string.unknown_location);
        holder.image.setImageResource(android.R.drawable.ic_dialog_map);
    }

    private void fillItemWithValidData(WeatherItemViewHolder holder, WeatherResponse weatherResponse) {
        final WeatherItem weatherItem = weatherResponse.weather[0];
        if (weatherItem != null) {
            holder.description.setText(weatherItem.description);
            Picasso.with(holder.image.getContext())
                    .load(String.format(Locale.ENGLISH, RetrofitClient.BASE_IMAGE_URL, weatherItem.icon))
                    .into(holder.image);
        }
        holder.text.setText(weatherResponse.name);
    }

    /**
     * +1 extra count because
     *
     * @return all items count
     */
    @Override
    public int getItemCount() {
        return mItemsList.size() + EXTRA_ITEM_COUNT;
    }

    /**
     * Method updates current weather cell with valid data.<p>
     * current weather position always at 0 position.
     *
     * @param weatherResponse weather data container.
     */
    public void updateCurrentLocationData(final WeatherResponse weatherResponse) {
        mItemsList.set(0, weatherResponse);
        notifyItemChanged(0);
    }


    /**
     * Method adds new city weather to the list.
     *
     * @param weatherResponse weather data container.
     */
    public void addCity(final WeatherResponse weatherResponse) {
        mItemsList.add(weatherResponse);
        notifyItemChanged(mItemsList.size() - 1);
    }


    /**
     * View Holder represents UI for main cell
     */
    public static class WeatherItemViewHolder extends RecyclerView.ViewHolder {
        public final TextView text;
        public final TextView description;
        public final ImageView image;

        public WeatherItemViewHolder(View v) {
            super(v);
            text = (TextView) v.findViewById(R.id.item_text);
            description = (TextView) v.findViewById(R.id.item_description);
            image = (ImageView) v.findViewById(R.id.item_image);
        }
    }

    /**
     * View Holder represents UI add new city control
     */
    public static class AddNewItemViewHolder extends RecyclerView.ViewHolder {
        public final View addNewCity;

        public AddNewItemViewHolder(View v) {
            super(v);
            addNewCity = v.findViewById(R.id.add_new_city);
        }
    }

    /**
     * Callback interface notifies activity with actions from adapter
     */
    interface ActionsCallback {
        void addCitySelected(final View view);
    }

    @Override
    public void onClick(View v) {
        mActionsCallback.addCitySelected(v);
    }
}
