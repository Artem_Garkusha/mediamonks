package gark.ebaytask.network.requesthelper;

import android.content.Intent;
import android.location.Location;

import java.io.IOException;

import gark.ebaytask.model.WeatherResponse;
import gark.ebaytask.network.BaseApi;
import gark.ebaytask.network.NetworkIntentService;
import gark.ebaytask.network.RetrofitClient;
import retrofit.Call;
import retrofit.Response;

/**
 * Request helper contains logic with handling request and response weather data of current location.
 */
public class CurrentLocationHelper extends RequestHelper {

    public CurrentLocationHelper(final Intent intent) {
        super(intent);
    }

    @Override
    public void handleRequest() {
        final Location location = mIntent.getParcelableExtra(NetworkIntentService.LOCATION_DATA_EXTRA);
        final Call<WeatherResponse> item = RetrofitClient.getWeatherApi().getWeatherByLatLon(location.getLatitude(), location.getLongitude(), BaseApi.API_KEY);
        try {
            final Response<WeatherResponse> response = item.execute();
            deliverResultToReceiver(SUCCESS_RESULT, response.body());
        } catch (IOException e) {
            e.printStackTrace();
            deliverResultToReceiver(FAILURE_RESULT, null);
        }
    }
}
