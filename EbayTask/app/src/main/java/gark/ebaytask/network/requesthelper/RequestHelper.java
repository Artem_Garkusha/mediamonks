package gark.ebaytask.network.requesthelper;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import gark.ebaytask.model.WeatherResponse;
import gark.ebaytask.network.NetworkIntentService;

/**
 * Class is responsible for managing network request.
 */
public abstract class RequestHelper {

    public static final String RESULT_DATA_KEY = "RESULT_DATA_KEY";
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;

    protected Intent mIntent;

    public RequestHelper(final Intent intent) {
        mIntent = intent;
    }


    /**
     * Method where main network request should be created and handled.
     */
    public abstract void handleRequest();

    /**
     * Sends a resultCode and message to the receiver.
     */
    protected void deliverResultToReceiver(final int resultCode, final WeatherResponse weatherResponse) {
        final Bundle bundle = new Bundle();
        final ResultReceiver receiver = mIntent.getParcelableExtra(NetworkIntentService.RECEIVER);
        bundle.putParcelable(RESULT_DATA_KEY, weatherResponse);
        receiver.send(resultCode, bundle);
    }
}
