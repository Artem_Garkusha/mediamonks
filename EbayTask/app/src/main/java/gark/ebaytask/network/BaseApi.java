package gark.ebaytask.network;

import gark.ebaytask.model.WeatherResponse;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Interface with OpenWeatherMap API list.
 */
public interface BaseApi {

    String API_KEY = "f3e2e93e258b92c9cfd440422d9a17b9";

    @GET("data/2.5/weather")
    Call<WeatherResponse> getWeatherByLatLon(@Query("lat") double lat, @Query("lon") double lon, @Query("APPID") String key);

    @GET("data/2.5/weather")
    Call<WeatherResponse> getWeatherByName(@Query("q") String name, @Query("APPID") String key);

}
