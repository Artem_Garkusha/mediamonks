package gark.ebaytask.network.requesthelper;

import android.content.Intent;
import android.support.annotation.NonNull;

import gark.ebaytask.network.NetworkIntentService;

/**
 * Factory for {@code RequestHelper} creation.
 */
public class RequestHelperFactory {

    /**
     * method associate proper {@code RequestHelper} with incoming {@code Intent}
     *
     * @return RequestHelper
     */
    public static RequestHelper getProcessor(@NonNull final Intent intent) {
        if (intent.hasExtra(NetworkIntentService.CITY_NAME_DATA_EXTRA)) {
            return new CityNameHelper(intent);
        } else if (intent.hasExtra(NetworkIntentService.LOCATION_DATA_EXTRA)) {
            return new CurrentLocationHelper(intent);
        }
        return null;
    }
}
