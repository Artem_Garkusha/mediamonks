package gark.ebaytask.network;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.ResultReceiver;

import gark.ebaytask.network.requesthelper.RequestHelper;
import gark.ebaytask.network.requesthelper.RequestHelperFactory;

/**
 * IntentService is a base class for {@link Service}s that handle asynchronous
 * requests. Used for network operation execution.
 */
public class NetworkIntentService extends IntentService {

    public static final String TAG = "weather_network_service";
    public static final String RECEIVER = "RECEIVER";
    public static final String LOCATION_DATA_EXTRA = "LOCATION_DATA_EXTRA";
    public static final String CITY_NAME_DATA_EXTRA = "CITY_NAME_DATA_EXTRA";

    public NetworkIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        final RequestHelper processor = RequestHelperFactory.getProcessor(intent);
        if (processor != null) {
            processor.handleRequest();
        }

    }

    /**
     * Method starts {@code NetworkIntentService} for current weather requesting.
     *
     * @param location current location
     * @param receiver result receiver
     */
    public static void requestWeatherCurrentLocation(final Location location, final Context context, final ResultReceiver receiver) {
        final Intent intent = new Intent(context, NetworkIntentService.class);
        intent.putExtra(RECEIVER, receiver);
        intent.putExtra(LOCATION_DATA_EXTRA, location);
        context.startService(intent);
    }

    /**
     * Method starts {@code NetworkIntentService} for city weather requesting.
     *
     * @param cityName name of city
     * @param receiver result receiver
     */
    public static void requestWeatherByCityName(final String cityName, final Context context, final ResultReceiver receiver) {
        final Intent intent = new Intent(context, NetworkIntentService.class);
        intent.putExtra(RECEIVER, receiver);
        intent.putExtra(CITY_NAME_DATA_EXTRA, cityName);
        context.startService(intent);
    }
}
