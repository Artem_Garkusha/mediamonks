package gark.ebaytask.network;


import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Network engine is based on {@code Retrofit} library
 */
public class RetrofitClient {

    public static final String OPEN_WEATHER_MAP_API_URL = "http://api.openweathermap.org/";

    public static final String BASE_IMAGE_URL = "http://openweathermap.org/img/w/%s.png";

    public static BaseApi getWeatherApi() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(OPEN_WEATHER_MAP_API_URL)
                .build()
                .create(BaseApi.class);
    }
}
