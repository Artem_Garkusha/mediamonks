package gark.ebaytask;

import android.content.Intent;
import android.location.Location;
import android.test.InstrumentationTestCase;

import junit.framework.Assert;

import gark.ebaytask.network.NetworkIntentService;
import gark.ebaytask.network.requesthelper.CityNameHelper;
import gark.ebaytask.network.requesthelper.CurrentLocationHelper;
import gark.ebaytask.network.requesthelper.RequestHelper;
import gark.ebaytask.network.requesthelper.RequestHelperFactory;

/**
 * Test FactoryHelper functionality
 */
public class FactoryHelperTest extends InstrumentationTestCase {

    public void testCityNameRequestHelper() {
        Intent intent = new Intent();
        intent.putExtra(NetworkIntentService.CITY_NAME_DATA_EXTRA, "Amsterdam");
        RequestHelper requestHelper = RequestHelperFactory.getProcessor(intent);

        Assert.assertTrue(requestHelper instanceof CityNameHelper);
    }

    public void testCurrentLocationRequestHelper() {
        Location location = new Location("location");
        Intent intent = new Intent();
        intent.putExtra(NetworkIntentService.LOCATION_DATA_EXTRA, location);
        RequestHelper requestHelper = RequestHelperFactory.getProcessor(intent);

        Assert.assertTrue(requestHelper instanceof CurrentLocationHelper);
    }

    public void testRequestHelperEmptyIntent() {
        Intent intent = new Intent();
        RequestHelper requestHelper = RequestHelperFactory.getProcessor(intent);

        Assert.assertNull(requestHelper);
    }
}