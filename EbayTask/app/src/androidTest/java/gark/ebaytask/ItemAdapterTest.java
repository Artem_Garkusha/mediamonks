package gark.ebaytask;

import android.test.InstrumentationTestCase;

import junit.framework.Assert;

import java.util.ArrayList;

import gark.ebaytask.model.WeatherResponse;
import gark.ebaytask.ui.ItemAdapter;

/**
 * Tests for Items Adapter
 */
public class ItemAdapterTest extends InstrumentationTestCase {

    ArrayList<WeatherResponse> emptyList = new ArrayList<>();
    ArrayList<WeatherResponse> twoElementsList = new ArrayList<>();


    @Override
    protected void setUp() throws Exception {
        super.setUp();

        WeatherResponse weatherResponseA = new WeatherResponse();
        WeatherResponse weatherResponseB = new WeatherResponse();

        twoElementsList.add(weatherResponseA);
        twoElementsList.add(weatherResponseB);
    }

    /**
     * Edit text control 'add new city' always at bottom even if list is empty.
     */
    public void testEmptyListItemsCount() {
        ItemAdapter itemAdapter = new ItemAdapter(null, emptyList);
        Assert.assertEquals(1, itemAdapter.getItemCount());
    }

    /**
     * Two weather element plus bottom control
     */
    public void testNonEmptyListItemsCount() {
        ItemAdapter itemAdapter = new ItemAdapter(null, twoElementsList);
        Assert.assertEquals(3, itemAdapter.getItemCount());
    }

    public void testViewTypes() {
        ItemAdapter itemAdapter = new ItemAdapter(null, twoElementsList);

        Assert.assertEquals(ItemAdapter.TYPE_DEFAULT_WEATHER_ITEM, itemAdapter.getItemViewType(0));
        Assert.assertEquals(ItemAdapter.TYPE_DEFAULT_WEATHER_ITEM, itemAdapter.getItemViewType(1));
        Assert.assertEquals(ItemAdapter.TYPE_BOTTOM_ITEM, itemAdapter.getItemViewType(2));
    }

    /**
     * update current location with data does not change count of items in adapter
     */
    public void testUpdateCurrentLocation() {
        WeatherResponse weatherResponseC = new WeatherResponse();

        ItemAdapter itemAdapter = new ItemAdapter(null, twoElementsList);
        int countBefore = itemAdapter.getItemCount();

        itemAdapter.updateCurrentLocationData(weatherResponseC);
        int countAfter = itemAdapter.getItemCount();

        Assert.assertEquals(countBefore, countAfter);
    }

    /**
     * add new city does increase count of items in adapter +1
     */
    public void testNewAddCity() {
        WeatherResponse weatherResponseC = new WeatherResponse();

        ItemAdapter itemAdapter = new ItemAdapter(null, twoElementsList);
        int countBefore = itemAdapter.getItemCount();

        itemAdapter.addCity(weatherResponseC);
        int countAfter = itemAdapter.getItemCount();

        Assert.assertEquals(countBefore + 1, countAfter);
    }
}