package gark.ebaytask;

import android.os.Bundle;
import android.test.InstrumentationTestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import gark.ebaytask.model.WeatherResponse;
import gark.ebaytask.network.requesthelper.RequestHelper;
import gark.ebaytask.receivers.CityNameResultReceiver;
import gark.ebaytask.receivers.CurrentLocationResultReceiver;
import gark.ebaytask.ui.ItemAdapter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Test Result receivers behaviour
 */
@RunWith(MockitoJUnitRunner.class)
public class ResultReceiversTest extends InstrumentationTestCase {

    @Mock
    ItemAdapter mItemsAdapter;

    WeatherResponse mWeatherResponse = new WeatherResponse();
    Bundle mBundle = new Bundle();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
        MockitoAnnotations.initMocks(this);

        mBundle.putParcelable(RequestHelper.RESULT_DATA_KEY, mWeatherResponse);
    }


    @Test
    public void testCityDataWasReceivedSuccessfully() {
        CityNameResultReceiver resultReceiver = new CityNameResultReceiver(mItemsAdapter, null);
        resultReceiver.send(RequestHelper.SUCCESS_RESULT, mBundle);

        verify(mItemsAdapter, times(1)).addCity(mWeatherResponse);
    }


    @Test
    public void testCityDataWasNotReceivedSuccessfully() {
        CityNameResultReceiver resultReceiver = new CityNameResultReceiver(mItemsAdapter, null);
        resultReceiver.send(RequestHelper.FAILURE_RESULT, mBundle);

        verify(mItemsAdapter, times(0)).addCity(mWeatherResponse);
    }


    @Test
    public void testCurrentLocationDataWasReceivedSuccessfully() {
        CurrentLocationResultReceiver resultReceiver = new CurrentLocationResultReceiver(mItemsAdapter, null);
        resultReceiver.send(RequestHelper.SUCCESS_RESULT, mBundle);

        verify(mItemsAdapter, times(1)).updateCurrentLocationData(mWeatherResponse);
    }

    @Test
    public void testCurrentLocationDataWasNotReceivedSuccessfully() {
        CurrentLocationResultReceiver resultReceiver = new CurrentLocationResultReceiver(mItemsAdapter, null);
        resultReceiver.send(RequestHelper.FAILURE_RESULT, mBundle);

        verify(mItemsAdapter, times(0)).updateCurrentLocationData(mWeatherResponse);
    }

}